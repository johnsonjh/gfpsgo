module github.com/johnsonjh/gfpsgo

go 1.19

require (
	github.com/johnsonjh/leaktestfe v0.0.0-20230308203709-32b9101894df
	github.com/opencontainers/runc v1.1.5-0.20230326031107-27fb72c7ffdb
	github.com/pkg/errors v0.9.2-0.20201214064552-5dd12d0cfe7f
	github.com/sirupsen/logrus v1.9.0
	github.com/stretchr/testify v1.8.3-0.20230314120135-c5fc9d6b6b21
	golang.org/x/sys v0.6.1-0.20230317000753-00d8004a1448
)

require (
	github.com/davecgh/go-spew v1.1.2-0.20180830191138-d8f796af33cc // indirect
	github.com/kr/pretty v0.3.1 // indirect
	github.com/pmezard/go-difflib v1.0.1-0.20181226105442-5d4384ee4fb2 // indirect
	github.com/rogpeppe/go-internal v1.9.1-0.20230209130841-f0583b8402aa // indirect
	go.uber.org/goleak v1.2.2-0.20230213210001-751da596f6f7 // indirect
	gopkg.in/check.v1 v1.0.0-20201130134442-10cb98267c6c // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
